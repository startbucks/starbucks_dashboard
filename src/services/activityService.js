import axios from "axios";

const _axios = axios.create({
  baseURL: 'http://localhost:3000/',
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImFkbWluSWQiOjEsImFjY291bnQiOiJhZG1pbjAxIn0sImV4cCI6MTYyOTgxNDk5MiwiaWF0IjoxNjI5ODE0MDkyfQ.0e995rRLQyUvU9IHDHvf6CiIKEdrH0HKDWGFAqD_RKM'
  }
})

export function GET_activities(token) {
  return _axios.get('/activities',{
    headers:{
      Authorization: `bearer ${token}`,
    }
  })
}

export function GET_oneActivity(id,token) {
  console.log(id);
  return _axios.get(`/activities/${id}`,{
    headers:{
      Authorization: `bearer ${token}`,
    }
  })
}

export function POST_activity(payload,token) {
  return _axios.post('/activities', payload,{
    headers:{
      Authorization: `bearer ${token}`,
    }
  })
}

export function PATCH_activity(id, payload, token) {
  return _axios.put(`/activities/${id}`,payload,{
    headers:{
      Authorization: `bearer ${token}`,
    }
  })
}
