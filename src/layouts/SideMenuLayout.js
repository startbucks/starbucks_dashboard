import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {  Images } from '../theme';
import hashHistory from '../utils/hashHistory';
import {routerRedux}from 'dva/router'
import './SideMenuLayout.css';


const sideWidth = 230;
const styles = {
  content: {
    backgroundColor: '#3A3A3A',
    width: sideWidth,
    height: '100%',
    padding: '0px !important',
    display: 'flex',
    flexDirection:'column',
    alignItems: 'center',
    justifyContent:'space-between'
  },
  fill: {
    display: 'flex',
    width: '100%',
    height: '100%',
    flex: 1,
    // backgroundColor: '#EEEEEE',
    backgroundColor:'#DFEAE3' 
  },
};

let sideList = [
  {
    key: 'story',
    title: '簡介管理',
    img: Images.admin_black,
    check_img: Images.admin_white,
    url: '/story',
  },
  {
    key: 'activity',
    title: '活動管理',
    img: Images.user_black,
    check_img: Images.user_white,
    url: '/activity',
  },
  {
    key: 'meal',
    title: '餐點管理',
    img: Images.shop_black,
    check_img: Images.shop_white,
    url: '/food',
  },
  {
    key: 'drink',
    title: '飲料管理',
    img: Images.shop_black,
    check_img: Images.shop_white,
    url: '/drink',
  },
  // {
  //   key: 'admin',
  //   title: '人員管理',
  //   img: Images.shop_black,
  //   check_img: Images.shop_white,
  //   url: '/admin',
  // }
];

const mapStateToProps = state => {
  return {
      
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout(callback, loading) {
      dispatch({ type: 'login/logout', callback, loading });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class SideMenuLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      track: {
        title: '',
        href: '',
      }
    };
  }
  static propTypes = {
    class: PropTypes.object,
    children: PropTypes.object,
    classes: PropTypes.object,
  };

  componentDidMount() {
    this.showRoute();
    window.addEventListener("hashchange", this.showRoute, false);
  }

  componentWillUnmount() {
    window.removeEventListener("hashchange", this.showRoute, false);
  }

  //登出
  onLogout = () => {
    const { logout } = this.props;
    logout(() => this.props.goToRoute('/'), loading => this.setState({ loading }));
  };

  showRoute = () => {
    if (window.location.hash.indexOf('activity') > -1) {
      this.setState({
        title: '活動管理',
        href: '/#/activity',
      });
    } else if (window.location.hash.indexOf('story') > -1) {
      this.setState({
        title: '簡介管理',
        href: '/#/story',
      });
    } else if (window.location.hash.indexOf('food') > -1) {
      this.setState({
        title:'餐點管理',
        href: '/#/food',
      });
    } else if (window.location.hash.indexOf('drink') > -1){
      this.setState({
        title: '飲料管理',
        href: '/#/drink',
      });
    }
    // else if (window.location.hash.indexOf('person') > -1){
    //   this.setState({
    //     title: '人員管理',
    //     href: '/#/admin',
    //   });
    // }

  }

  render() {
    const { isOpen, track, title } = this.state;
    const { children, history, setToken, user, resetPassword } = this.props;
    return (
      <div style={styles.fill}>
        <div style={styles.content}>
            <div className="content_top">
                <img src={Images.logo} style={{marginTop:'50px',marginBottom:'50px',width:'100px',height:'100px'}}/>
                <ul>
                  {
                    sideList.map((item) =>
                    <li
                      className="icon_block"
                      style={{
                        backgroundColor: item.title=== title ? '#7D7D7D': 'transparent',
                        color: 'white',
                      }}
                      onClick={() => history.push(item.url)}
                    >
                      {item.title}
                    </li>
                    )
                  }
                </ul>
            </div>
            <ul>
              <li className="icon_block2" onClick={() => this.onLogout()}>
                <img className="logout"src={Images.logout} />登出
              </li>
            </ul>
        </div>

        <div style={{ width: `calc(100vw - ${sideWidth}px)`,display:'flex',alignItems:'center',overflowY:'auto'}}>
          {children}
        </div>
      </div>
    );
  }
}
)
