const TOKEN_KEY = 'token';

export function setToken(token) {
  return localStorage.setItem(TOKEN_KEY, token);
}

export function cleanToken() {
  return localStorage.removeItem(TOKEN_KEY);
}

export function getToken() {
  return localStorage.getItem(TOKEN_KEY);
}

export function getUserId() {
  return localStorage.getItem('userid');
}

export function getLanguage() {
  return localStorage.getItem('language')
}

export function setLanguage(value) {
  return localStorage.setItem('language', value)
}
