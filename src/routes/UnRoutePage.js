import React, { Fragment } from 'react';

import { Switch, Redirect } from 'dva/router';
import { Route, HashRouter } from 'dva/router';

// import EmptyLayout from 'src/layouts/EmptyLayout';
// import LoginScreen from 'src/containers/Login/LoginScreen';
import LoginScreen from '../containers/Login/LoginScreen';
import EmptyLayout from '../layouts/EmptyLayout'

const RouterPage = () => (
  <EmptyLayout>
    <HashRouter>
      <Switch>
        <Route path="/login" component={LoginScreen} />
        <Redirect from="/" to="/login" />
      </Switch>
    </HashRouter>
  </EmptyLayout>
);



class UnRoutePage extends React.Component {
  static propTypes = {};

  static defaultProps = {};

  render() {
    return (
      <Fragment>
        <RouterPage />
      </Fragment>
    );
  }
}
export default UnRoutePage;
