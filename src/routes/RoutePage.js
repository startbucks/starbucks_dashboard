import React, { Fragment } from 'react';

import { Route, Switch, routerRedux, withRouter, HashRouter, Redirect } from "dva/router";

import SideLayoutContainer from '../layouts/SideMenuLayout';
import activityScreen from '../containers/Activity/activityScreen'
import activityDetail from '../containers/Activity/activityDetail'
import activityCreate from '../containers/Activity/activityCreate'
import drinkScreen from '../containers/Drink/drinkScreen'
import drinkCreate from '../containers/Drink/drinkCreate'
import drinkDetail from '../containers/Drink/drinkDetail'
import foodScreen from '../containers/Food/foodScreen'
import foodCreate from '../containers/Food/foodCreate'
import foodDetail from '../containers/Food/foodDetail'
import storyScreen from '../containers/Story/storyScreen'
// import adminScreen from '../containers/Admin/adminScreen'
// import adminCreate from '../containers/Admin/adminCreate'

const { ConnectedRouter } = routerRedux;

class RouterPage extends React.Component {
  render() {
    const { children,history}=this.props
    return children;
    {/*return (
      
      
    )*/}
  }
}

const RouterRoot = withRouter(RouterPage);

// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  return (
    <ConnectedRouter history={props.history}>
      <RouterRoot {...props}> 
        <SideLayoutContainer {...props}>
          <HashRouter>
            <Switch>
              <Route exact path="/activity" component={activityScreen} />
              <Route exact path="/activity/detail/:id" component={activityDetail}/>
              <Route exact path="/activity/create" component={activityCreate}/>
{/*              <Route exact path="/admin" component={adminScreen}/>
  <Route exact path="/admin/create" component={adminCreate}/>*/}
              <Route exact path="/drink" component={drinkScreen} />
              <Route exact path="/drink/create" component={drinkCreate}/>
              <Route exact path="/drink/detail/:id" component={drinkDetail}/>
              <Route exact path="/food" component={foodScreen} />
              <Route exact path="/food/create" component={foodCreate}/>
              <Route exact path="/food/detail/:id" component={foodDetail}/>
              <Route exact path="/story" component={storyScreen} />
              <Redirect from="/" to="/activity" />
            </Switch>
          </HashRouter>
        </SideLayoutContainer>
      </RouterRoot>
    </ConnectedRouter>
  );
};

