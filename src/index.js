import dva from 'dva';

// 1. Initialize
const app = dva();

// 2. Plugins
// app.use({});

// 3. Model
app.model(require('./models/appModel').default);
app.model(require('./models/loginModel').default);
app.model(require('./models/foodModel').default);
app.model(require('./models/drinkModel').default);
app.model(require('./models/activityModel').default);

// 4. Router
app.router(require('./router').default);

// 5. Start
app.start('#root');