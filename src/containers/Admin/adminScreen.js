import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { bindActionCreators } from 'redux';
import {  Images } from '../../theme';
import { Table, Tag, Space } from 'antd';
// import './AdminScreen.css';
// import hashHistory from 'src/utils/HashHistory';
import moment from 'moment';


const styles = {
  root: {
    flexGrow: 1,
    height: '100%',
    overflowY: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    overflowY: 'auto'
  },
  fullHeight: {
    height: '100%',
  },
  wrapper: {
    width: '90%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  contentBottom: {
    width: '100%',
    marginTop: '80px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    fontSize:'20px',
    fontWeight:'bold'
  },
  contentBottomTitle:{
    marginLeft:'5px',
    marginBottom:'10px',
    display:'flex',
    alignItems:'center',
    cursor:'pointer'
  }
};


const handleUse = (value) => {
  if (value) {
    return '#4596B9';
  } else if (!value) {
    return '#DD614A';
  }
}

const handleUseText = (value) => {
  if (value) {
    return '使用中';
  } else if (!value) {
    return '已停用';
  }
}

const handleViewUpdate = (id) => {
//   hashHistory.push(`/admin/update/${id}`);
}

const columns = [
  {
    width: '30%',
    title: '活動日期',
    dataIndex: 'user_id',
    key: 'user_id',
    align:'center',
    render: (value, record) => (
      <img src={Images.edit_gray} className="imgEdit" onClick={() => handleViewUpdate(record.user_id)} />
    ),
  },
  {
    width: '40%',
    title: '活動名稱',
    dataIndex: 'user_account',
    key: 'user_account',
    align:'center',
    render: text => <div style={{ fontWeight: '500', color: '#252525' }}>{text}</div>,
  },
  {
    width: '20%',
    title: '發佈者',
    dataIndex: 'name',
    key: 'name',
    align:'center',
  },
  {
    width: '10%',
    title: '設定',
    dataIndex: 'create_time',
    key: 'create_time',
    align:'center',
    render: (value, record) => <div style={{ fontWeight: '500', color: '#252525' }}>{moment(record.create_time).format('YYYY-MM-DD')}</div>,
  },
];

const data = [
  {
    key: '1',
    account: 'Koala1',
    name: 'Koala',
    createTime: '2021/07/21',
    statusTags: ['使用中'],
  },
  {
    key: '1',
    account: 'Koala2',
    name: 'Koala',
    createTime: '2021/07/22',
    statusTags: ['停用'],
  },
  {
    key: '3',
    account: 'Koala3',
    name: 'Koala',
    createTime: '2021/07/23',
    statusTags: ['使用中'],
  },
  {
    key: '4',
    account: 'Koala4',
    name: 'Koala',
    createTime: '2021/07/24',
    statusTags: ['使用中'],
  },
  {
    key: '5',
    account: 'Koala5',
    name: 'Koala',
    createTime: '2021/07/25',
    statusTags: ['使用中'],
  },
  {
    key: '6',
    account: 'Koala6',
    name: 'Koala',
    createTime: '2021/07/23',
    statusTags: ['使用中'],
  },
  {
    key: '7',
    account: 'Koala7',
    name: 'Koala',
    createTime: '2021/07/24',
    statusTags: ['使用中'],
  },
  {
    key: '8',
    account: 'Koala8',
    name: 'Koala',
    createTime: '2021/07/25',
    statusTags: ['使用中'],
  },
];


const mapStateToProps = state => {
  return {

  };
};

const mapDispatchToProps = dispatch => {
  return {

  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class adminScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {


  }

  render() {
    const { adminData } = this.props;

    return (
      <React.Fragment>
        <div style={{ width: '100%', height: '100%' }}>
          <div style={styles.root}>
            <div style={styles.wrapper}>
              <div style={styles.contentBottom}>
                <div style={styles.contentBottomTitle}>
                  <img src={Images.insert}  style={{width:'25px',marginRight:'10px'}}/>新增人員
                </div>
                <Table columns={columns} dataSource={adminData} style={{ width: '100%',textAlign:'center'}} />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
);
