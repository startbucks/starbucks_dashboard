import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {Form , Button, Radio, DatePicker, message,Input} from 'antd';
import { bindActionCreators } from 'redux';
import {  Images } from '../../theme';
import moment from 'moment';
import { FormInput,FormInputSelect } from '../../components';
import hashHistory from '../../utils/hashHistory';

const styles = {
  formStoreUpdate:{
    width:'70%',
    height:'520px',
    marginBottom:'5%',
    display:'flex',
    justifyContent:'flex-start',
    flexWrap:'wrap',

    backgroundColor:'#FFFFFF',  
    padding:'7%',
    paddingTop:'3%',
    boxShadow:'5px 5px #E4E2E2',
  },
  
  title:{
    fontSize:'22px',
    fontWeight:'bold',
    color:'#006439',
    display:'flex',
    alignItems:'center',
    width:'100%',
    marginBottom:'30px',
  },
  photo:{
    display: 'block',
    width: '150px',
    height: '150px',
    marginRight: '6%', 
    borderRadius: '50%',
    overflow: 'hidden',
  },
  photoImg:{
    top: '0',
    bottom: '0',
    right: '0',
    left: '0',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center',
  },
  leftColStyle:{
    height:'40px',
    textAlign:'right',
    paddingRight:'15px',
    color:'#8A8A8A'
  },
  rightColStyle:{
    height:'38px',
  },
  btnStyle: {
    width: '150px',
    height: '38px',
    backgroundColor: '#00643C',
    borderRadius: '10px',
    color: 'white',
    float:'right',
  },

};


const handleUse = (value) => {
  if (value) {
    return '#4596B9';
  } else if (!value) {
    return '#DD614A';
  }
}

const handleUseText = (value) => {
  if (value) {
    return '使用中';
  } else if (!value) {
    return '已停用';
  }
}

const handleViewUpdate = (id) => {
//   hashHistory.push(`/admin/update/${id}`);
}


const layout = {
  labelCol: { span: 6},
  wrapperCol: { span: 18 },
};




const mapStateToProps = state => {
  return {

  };
};

const mapDispatchToProps = dispatch => {
  return {

  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class adminCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {


  }

  
  disabledDate(current) {
    return current && current < moment().endOf('day');
  }

  render() {
    const { adminData } = this.props;
    
    const adminCreateData ={
      admin_account:'',
      admin_name:'',
      admin_password:'',
    }

    return (
      <React.Fragment>
        <div style={{width: '100%', height: '100%',display:'flex',justifyContent:'center',alignItems:'center',overflowY:'auto',paddingTop:'30px'}}>
          <div style={styles.formStoreUpdate}>
            <div style={styles.title}>
              <img 
                src={Images.back} 
                onClick={() => hashHistory.goBack()} 
                style={{width:'25px',height:'18px',marginRight:'10px',cursor:'pointer'}}
              />
              返回人員列表
            </div>


            <div style={{width:'100%'}}>
              <Form
                {...layout}
                name="basic"
                style={{width:'90%',margin:'auto'}}
                onFinish={value=>this.handleUpdate(value)}
              >
              <FormInput
                required
                title="帳號"
                propName='admin_account'
                placeholder="帳號"
                requiredErrorMessage="請輸入帳號"
              />
              <FormInput
                required
                title="名稱"
                propName='admin_name'
                placeholder="名稱"
                requiredErrorMessage="請輸入名稱"
              />
              <Form.Item
                name="user_pwd"
                label="密碼"
                dependencies={['user_pwd']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: '請輸入密碼',
                  },
                ]}
              ><Input.Password style={{ marginLeft: '5px', borderRadius: '8px' }} />
              </Form.Item>
              <Form.Item
                name="confirm"
                label="密碼確認"
                dependencies={['user_pwd']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: '請輸入確認密碼',
                  },
                  ({ getFieldValue }) =>
                  ({
                    validator(_, value) {
                      if (!value || getFieldValue('user_pwd') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('輸入內容與密碼不一致'));
                    },
                  }),
                ]}
              ><Input.Password style={{ marginLeft: '5px', borderRadius: '8px' }} />
              </Form.Item>
                
                
                <Button style={styles.btnStyle} htmlType="submit">
                  新增
                </Button>

              </Form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
);
