import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { bindActionCreators } from 'redux';
import {  Images } from '../../theme';
import { Table, Tag, Space } from 'antd';
import moment from 'moment';
import _ from 'lodash';


const styles = {
  root: {
    flexGrow: 1,
    height: '100%',
    overflowY: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    overflowY: 'auto'
  },
  fullHeight: {
    height: '100%',
  },
  wrapper: {
    width: '90%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  contentBottom: {
    width: '100%',
    marginTop: '80px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    fontSize:'20px',
    fontWeight:'bold'
  },
  contentBottomTitle:{
    marginLeft:'5px',
    marginBottom:'10px',
    display:'flex',
    alignItems:'center',
    cursor:'pointer'
  }
};

const handleViewUpdate = (id) => {
//   hashHistory.push(`/admin/update/${id}`);
}



const mapStateToProps = state => {
  return {
    drinkData: _.get(state, 'drink.drinkData', []),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_drinks(){
      dispatch({ type: 'drink/GET_drinks' });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class drinkScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {
    const { GET_drinks } = this.props;
    GET_drinks();
    

  }

  render() {
    const { drinkData } = this.props;
    console.log(drinkData)

    const columns = [
      {
        width: '30%',
        title: '飲料名稱',
        dataIndex: 'beverageName',
        key: 'beverageName',
        align:'center',
        render: (value, record) => (
          <div>
            {(record.beverageName)}
          </div>
        ),
      },
      {
        width: '40%',
        title: '類別',
        dataIndex: 'typeId',
        key: 'typeId',
        align:'center',
        render: (value, record) => (
          <div>
            {(record.type.typeName)}
          </div>
        ),
      },
      {
        width: '20%',
        title: '狀態',
        dataIndex: 'status',
        key: 'status',
        align:'center',
        render: (value, record) => (
          <div>
            {(record.status)?"上架":"下架"}
          </div>
        ),
      },
      {
        width: '10%',
        title: '設定',
        dataIndex: 'beverageId',
        key: 'beverageId',
        align:'center',
        render: (value, record) => 
        <img 
          src={Images.edit} 
          onClick={()=>this.props.history.push(`/drink/detail/${record.beverageId}`)}>
      </img>,
      },
    ];
    return (
      <React.Fragment>
        <div style={{ width: '100%', height: '100%' }}>
          <div style={styles.root}>
            <div style={styles.wrapper}>
              <div style={styles.contentBottom}>
                <div style={styles.contentBottomTitle} onClick={()=>this.props.history.push('/drink/create')}>
                  <img src={Images.insert}  style={{width:'25px',marginRight:'10px'}} />新增飲料
                </div>
                <Table columns={columns} dataSource={drinkData} style={{ width: '100%',textAlign:'center'}} />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
);
