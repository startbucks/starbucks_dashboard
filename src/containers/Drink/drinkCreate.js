import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {Form , Button, Radio, DatePicker, message,Input,Select} from 'antd';
import { bindActionCreators } from 'redux';
import {  Images } from '../../theme';
import moment from 'moment';
import { FormInput,FormInputSelect } from '../../components';

const styles = {
  formStoreUpdate:{
    width:'70%',
    height:'700px',
    marginBottom:'5%',
    display:'flex',
    justifyContent:'flex-start',
    flexWrap:'wrap',

    backgroundColor:'#FFFFFF',  
    padding:'7%',
    paddingTop:'3%',
    boxShadow:'5px 5px #E4E2E2',
  },
  
  title:{
    fontSize:'22px',
    fontWeight:'bold',
    color:'#006439',
    display:'flex',
    alignItems:'center',
    width:'100%',
    marginBottom:'30px',
  },
  photo:{
    display: 'block',
    width: '150px',
    height: '150px',
    marginRight: '6%', 
    borderRadius: '50%',
    overflow: 'hidden',
  },
  photoImg:{
    top: '0',
    bottom: '0',
    right: '0',
    left: '0',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center',
  },
  leftColStyle:{
    height:'40px',
    textAlign:'right',
    paddingRight:'15px',
    color:'#8A8A8A'
  },
  rightColStyle:{
    height:'38px',
  },
  btnStyle: {
    width: '150px',
    height: '38px',
    backgroundColor: '#00643C',
    borderRadius: '10px',
    color: 'white',
    float:'right',
  },

};


const handleUseText = (value) => {
  if (value) {
    return '使用中';
  } else if (!value) {
    return '已停用';
  }
}

const layout = {
  labelCol: { span: 6},
  wrapperCol: { span: 18 },
};




const mapStateToProps = state => {
  return {

  };
};

const mapDispatchToProps = dispatch => {
  return {
    POST_drink(payload) {
      dispatch({ type: 'drink/POST_drink',payload });
    },
    GET_drinks() {
      dispatch({ type: 'drink/GET_drinks' });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class drinkCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {


  }

  
  disabledDate(current) {
    return current && current < moment().endOf('day');
  }

  handleCreate=(value)=>{
    console.log(this)
    console.log(value)
    const { POST_drink, GET_drinks} = this.props;
    this.setState({
      loading: true,
    });
    const callback = () => {
      this.setState({
        loading: false,
      })
    }
      
    POST_drink(value);
    GET_drinks();
    this.props.history.push('/drink')
  }

  
  render() {
    const { history} = this.props;

    const { Option } = Select;

    const drinkList=[
      {
        id:1,
        name:"經典咖啡飲料",
      }, 
      {
        id:2,
        name:"茶瓦納",
      },
      {
        id:3,
        name:"星冰樂",
      },
      {
        id:4,
        name:"冷萃咖啡",
      },
      {
        id:5,
        name:"其他飲料",
      },
      {
        id:6,
        name:"罐裝飲料",
      }
    ]

    const sizeList=[
      {
        id:"小杯",
        name:"小杯",
      }, 
      {
        id:"中杯",
        name:"中杯",
      },
      {
        id:"大杯",
        name:"大杯",
      },
      {
        id:"特大杯",
        name:"特大杯",
      },
    ]
   
    return (
      <React.Fragment>
        <div style={{width: '100%', height: '100%',display:'flex',justifyContent:'center',overflowY:'auto',paddingTop:'30px'}}>
          <div style={styles.formStoreUpdate}>
            <div style={styles.title} onClick={() => history.push('/drink')} >
              <img 
                src={Images.back} 
                
                style={{width:'25px',height:'18px',marginRight:'10px',cursor:'pointer'}}
                
              />
              返回飲料列表
            </div>


            <div style={{width:'100%'}}>
              <Form
                {...layout}
                name="basic"
                style={{width:'90%',margin:'auto'}}
                onFinish={value=>this.handleCreate(value)}
              >
                <FormInputSelect
                  title="飲料類別"
                  required
                  options={drinkList}
                  propName='typeId'
                  placeholder="請選擇類別"
                  requiredErrorMessage="請選擇類別"
                />

                <FormInput
                  required
                  title="飲料名稱"
                  propName='beverageName'
                  placeholder="飲料名稱"
                  requiredErrorMessage="請輸入飲料名稱"
                />

                <FormInput
                  required
                  title="飲料英文名稱"
                  propName='beverageEnglishName'
                  placeholder="飲料英文名稱"
                  requiredErrorMessage="請輸入飲料英文名稱"
                />

                <FormInputSelect
                  title="飲料尺寸"
                  required
                  options={sizeList}
                  propName='size'
                  placeholder="請選擇尺寸"
                  requiredErrorMessage="請選擇尺寸"
                />

                <FormInput
                  required
                  title="飲料價格"
                  propName='beveragePrice'
                  placeholder="飲料價格"
                  requiredErrorMessage="請輸入飲料價格"
                />
                <Form.Item name='beverageDescription' label="商品簡介"  rules={[{ required: true, message: '請輸入商品簡介' }]}>
                  <textarea style={{resize:'none',height:'100px',width:'100%',borderRadius:'8px',border:'1px solid #00643C'}}/>
                </Form.Item>

                <Button style={styles.btnStyle} htmlType="submit">
                  新增
                </Button>

              </Form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
);
