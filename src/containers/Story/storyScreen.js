import { Component } from 'react';
import { connect } from 'dva';
import _ from 'lodash';
import { Images } from '../../theme';
import { Row, Col, Button, Form, Input } from 'antd';
import FormItem from 'antd/lib/form/FormItem';


const styles={
  root: {
    flexGrow: 1,
    // height: '100%',
    overflowY: 'auto',
    display:'flex',
    justifyContent:'center',
    alignItems:'center'
  },
  card:{
    width:'80%',
    height:'80vh',
    backgroundColor:'white',
    boxShadow:"0px 0px 6px  #cccc",
    display:'flex',
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'space-evenly',

  },
  formStyle:{
    width:'60%',
  },
  inputStyle:{
    width:'100%',
    height:'25px',
    border:'0px',
    // borderBottom:'1px solid #d6d6d6',
    // backgroundColor:'transparent',
    borderRadius:'5px',
    color:'black',
    // marginLeft:'5%'
  },
  btnStyle:{
    width:'100%',
    height:'40px',
    border:'0px',
    borderRadius:'5px',
    backgroundColor:'#00643C',
    color:'white',
    fontSize:'16px',
    fontWeight: '800',
    display:'flex',
    alignItems:'center',
    justifyContent:'center',
    marginTop:'30px'
  }
};

const mapStateToProps = state => {
  return {
    companyData: _.get(state, 'company.companyData'),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_company() {
      dispatch({ type: 'company/GET_company' });
    },
    PATCH_company(payload) {
      dispatch({ type: 'company/PATCH_company',payload });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class storyScreen extends Component {
    constructor(props) {
      super(props);

      this.state = {
        loading: false,
      };
    }

    componentDidMount() {
      const {GET_company,companyData} =this.props;
      GET_company()
      console.log("companyData=>",companyData)
    }

    handleUpdate=(value)=>{
      console.log(value)
      const { PATCH_company,GET_company } = this.props;
      this.setState({
        loading: true,
      });
      const callback = () => {
        this.setState({
          loading: false,
        })
      }

      let formData = new FormData();
      formData.append('companySlogan', value.companySlogan);
      formData.append('description', value.description);
      PATCH_company(value, callback, this.state.userId);
      GET_company()
    }

    render() {
      const {companyData} =this.props;
      let slogan=""
      let description=""
      if (_.isEmpty(companyData)) {
        // return null;
      }else{
        slogan=companyData.companySlogan
        description=companyData.description
      }
      
      return (
        <div style={styles.root}>
          <Form 
            style={styles.card}
            name="basic"
            initialValues={{
              companyData
            }}
            onFinish={this.handleUpdate}
          >
            <button 
              style={{backgroundColor:'#006439',color:'white',border:'0px',borderRadius:'8px',width:'100px',height:'40px',marginTop:'50px',marginLeft:'80%'}} 
              htmlType="submit"
            >
              儲存修改
            </button>
            <div style={{margin:'auto',width:'80%',height:'20%',marginTop:'30px',marginBottom:'0px'}}>
                <Row style={{height:'100%'}}>
                    <Col xs={24} sm={24} md={24} lg={5} xl={5} 
                        style={{
                            color:'#207955',
                            fontSize:'25px',
                            fontWeight:'bold'
                        }}
                    >
                        公司slogan
                    </Col>
                    <Col  xs={24} sm={24} md={24} lg={18} xl={18} style={{height:'80%'}}>
                      <Form.Item name="companySlogan">
                        <textarea 
                          name="companySlogan"
                          defaultValue={slogan}
                          style={{
                              fontSize:'20px',
                              height:'100%',
                              width:'100%',
                              border:'2px solid #006439',
                              borderRadius:'8px',
                              resize:'none'
                          }}>
                        </textarea>
                      </Form.Item>
                        
                    </Col>
                </Row>    
            </div>
            <div style={{margin:'auto',width:'80%',height:'50%'}}>
                <Row style={{height:'100%'}}>
                  <Col xs={24} sm={24} md={24} lg={5} xl={5} 
                      style={{
                          color:'#207955',
                          fontSize:'25px',
                          fontWeight:'bold'
                      }}
                  >
                      公司介紹
                  </Col>
                  <Col xs={24} sm={24} md={24} lg={18} xl={18} style={{height:'90%'}}>
                    <Form.Item name="description" >
                      <textarea 
                        name="description" 
                        defaultValue={description}
                        style={{
                            fontSize:'20px',
                            height:'200px',
                            width:'100%',
                            border:'2px solid #006439',
                            borderRadius:'8px',
                            resize:'none'
                        }}
                      >
                      </textarea>
                    </Form.Item>    
                  </Col>
                </Row>
            </div>
          </Form>
      </div>
      );
    }
  },
);
