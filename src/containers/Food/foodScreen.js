import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { bindActionCreators } from 'redux';
import {  Images } from '../../theme';
import { Table, Tag, Space } from 'antd';
import moment from 'moment';
import _ from 'lodash';


const styles = {
  root: {
    flexGrow: 1,
    height: '100%',
    overflowY: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    overflowY: 'auto'
  },
  fullHeight: {
    height: '100%',
  },
  wrapper: {
    width: '90%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  contentBottom: {
    width: '100%',
    marginTop: '80px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    fontSize:'20px',
    fontWeight:'bold'
  },
  contentBottomTitle:{
    marginLeft:'5px',
    marginBottom:'10px',
    display:'flex',
    alignItems:'center',
    cursor:'pointer'
  }
};

const mapStateToProps = state => {
  return {
    foodData: _.get(state, 'food.foodData'),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_foods() {
      dispatch({ type: 'food/GET_foods' });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class foodScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {
    const {GET_foods, foodData} =this.props;
    GET_foods()
    console.log("foodData=>",foodData)
  }

  render() {
    const { foodData, history} = this.props;

    const columns = [
      {
        width: '40%',
        title: '商品名稱',
        dataIndex: 'foodName',
        key: 'foodName',
        align:'center',
        render: (value, record) => (
          <div>
            {record.foodName}
          </div>
        ),
      },
      {
        width: '30%',
        title: '類別',
        dataIndex: 'type',
        key: 'type',
        align:'center',
        render: (value, record) => (
          <div>
            {record.type.typeName}
          </div>
        ),
      },
      {
        width: '20%',
        title: '價格',
        dataIndex: 'foodPrice',
        key: 'foodPrice',
        align:'center',
        render: (value, record) => (
          <div>
            {record.foodPrice}
          </div>
        ),
      },
      {
        width: '10%',
        title: '設定',
        dataIndex: 'foodId',
        key: 'foodId',
        align:'center',
        render: (value, record) => 
            <img 
              src={Images.edit} 
              onClick={()=>this.props.history.push(`/food/detail/${record.foodId}`)}>
          </img>,
      },
    ];

    return (
      <React.Fragment>
        <div style={{ width: '100%', height: '100%' }}>
          <div style={styles.root}>
            <div style={styles.wrapper}>
              <div style={styles.contentBottom}>
                <div style={styles.contentBottomTitle}  onClick={()=>history.push('/food/create')}>
                  <img src={Images.insert}  style={{width:'25px',marginRight:'10px'}}/>新增餐點
                </div>
                <Table columns={columns} dataSource={foodData} style={{ width: '100%',textAlign:'center'}} />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
);
