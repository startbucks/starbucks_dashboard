import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {Form , Button, Radio, DatePicker, message,Input,Select} from 'antd';
import { bindActionCreators } from 'redux';
import {  Images } from '../../theme';
import moment from 'moment';
import { FormInput,FormInputSelect } from '../../components';
import hashHistory from '../../utils/hashHistory';
import _ from 'lodash';

const styles = {
  formStoreUpdate:{
    width:'70%',
    height:'700px',
    marginBottom:'5%',
    display:'flex',
    justifyContent:'flex-start',
    flexWrap:'wrap',

    backgroundColor:'#FFFFFF',  
    padding:'7%',
    paddingTop:'3%',
    boxShadow:'5px 5px #E4E2E2',
  },
  
  title:{
    fontSize:'22px',
    fontWeight:'bold',
    color:'#006439',
    display:'flex',
    alignItems:'center',
    width:'100%',
    marginBottom:'30px',
  },
  photo:{
    display: 'block',
    width: '150px',
    height: '150px',
    marginRight: '6%', 
    borderRadius: '50%',
    overflow: 'hidden',
  },
  photoImg:{
    top: '0',
    bottom: '0',
    right: '0',
    left: '0',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center',
  },
  leftColStyle:{
    height:'40px',
    textAlign:'right',
    paddingRight:'15px',
    color:'#8A8A8A'
  },
  rightColStyle:{
    height:'38px',
  },
  btnStyle: {
    width: '150px',
    height: '38px',
    backgroundColor: '#00643C',
    borderRadius: '10px',
    color: 'white',
    float:'right',
  },

};


const layout = {
  labelCol: { span: 6},
  wrapperCol: { span: 18 },
};


const mapStateToProps = state => {
  return {
    oneFood: _.get(state, 'food.oneFood'),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_food(id) {
      dispatch({ type: 'food/GET_food', id });
    },
    PATCH_food(id,payload) {
      dispatch({ type: 'food/PATCH_food',id ,payload });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class foodDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {
    const { history, GET_food, oneFood } =this.props;
    let temp = ""
    temp = history.location.pathname.split('/food/detail/');
    // let payload = {"foodId": temp[1]};
    GET_food(temp[1])
  }

  componentWillUnmount() {
    const { GET_food } = this.props;
    GET_food();
  }
  
  disabledDate(current) {
    return current && current < moment().endOf('day');
  }

  handleUpdate=(value)=>{
    console.log(value)
    const { PATCH_food,GET_food, history} = this.props;
    this.setState({
      loading: true,
    });
    const callback = () => {
      this.setState({
        loading: false,
      })
    }
    let temp = ""
    temp = history.location.pathname.split('/food/detail/');
    console.log(temp[1]);
    PATCH_food(temp[1],value, callback, this.state.userId);
    window.location.reload();
    GET_food(temp[1]);
    
  }
 
  render() {
    const { oneFood,history} = this.props;
    if (_.isEmpty(oneFood)) {
      return null;
    }
    console.log("oneFood=>",oneFood)
    const foodList=[
      {
        id:7,
        name:"星早餐",
      }, 
      {
        id:8,
        name:"星想餐",
      },
      {
        id:9,
        name:"午茶那堤組合",
      },
      {
        id:10,
        name:"輕食",
      },
      {
        id:11,
        name:"麵包/點心",
      },
      {
        id:12,
        name:"蛋糕甜點",
      },
      {
        id:13,
        name:"包裝食品",
      },
      {
        id:14,
        name:"蛋糕預購",
      },
      {
        id:15,
        name:"新鮮現烤",
      },
      {
        id:16,
        name:"中秋節預購",
      }
    ]

    const statusList = [{ id: true, name: "上架" }, { id: false, name: "下架" }]

    return (
      <React.Fragment>
        <div style={{width: '100%', height: '100%',display:'flex',justifyContent:'center',overflowY:'auto',paddingTop:'30px'}}>
          <div style={styles.formStoreUpdate} >
            <div style={styles.title} onClick={() => history.push('/food')} >
              <img 
                src={Images.back} 
                
                style={{width:'25px',height:'18px',marginRight:'10px',cursor:'pointer'}}
                
              />
              返回餐點列表
            </div>


            <div style={{width:'100%'}}>
              <Form
                {...layout}
                name="basic"
                style={{width:'90%',margin:'auto'}}
                onFinish={this.handleUpdate}
                initialValues={{
                  typeId:oneFood.type.typeId,
                  foodName:oneFood.foodName,
                  foodEnglishName:oneFood.foodEnglishName,
                  foodPrice:oneFood.foodPrice,
                  foodDescription:oneFood.foodDescription,
                  status:oneFood.status
                }}
              >
                <FormInputSelect
                  title="餐點類別"
                  required
                  options={foodList}
                  propName='typeId'
                  placeholder="請選擇類別"
                  requiredErrorMessage="請選擇類別"
                />

                <FormInput
                  required
                  title="餐點名稱"
                  propName='foodName'
                  placeholder="餐點名稱"
                  requiredErrorMessage="請輸入餐點名稱"
                  
                />

                <FormInput
                  required
                  title="餐點英文名稱"
                  propName='foodEnglishName'
                  placeholder="餐點英文名稱"
                  requiredErrorMessage="請輸入餐點英文名稱"
                />

                <FormInput
                  required
                  title="餐點價格"
                  propName='foodPrice'
                  placeholder="餐點價格"
                  requiredErrorMessage="請輸入餐點價格"
                />

                <FormInputSelect
                  required
                  title="狀態"
                  options={statusList}
                  propName='status'
                  placeholder="狀態"
                  requiredErrorMessage="請輸入餐點狀態"
                />

                <Form.Item name='foodDescription' label="餐點簡介"  rules={[{ required: true, message: '請輸入餐點簡介' }]}>
                  <textarea style={{resize:'none',height:'100px',width:'100%',borderRadius:'8px',border:'1px solid #00643C'}}/>
                </Form.Item>

                <Button style={styles.btnStyle} htmlType="submit">
                  確定修改
                </Button>

              </Form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
);
