import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {Form , Button, Radio, DatePicker, message,Input,Select} from 'antd';
import { bindActionCreators } from 'redux';
import {  Images } from '../../theme';
import moment from 'moment';
import { FormInput,FormInputSelect } from '../../components';

const styles = {
  formStoreUpdate:{
    width:'70%',
    height:'700px',
    marginBottom:'5%',
    display:'flex',
    justifyContent:'flex-start',
    flexWrap:'wrap',

    backgroundColor:'#FFFFFF',  
    padding:'7%',
    paddingTop:'3%',
    boxShadow:'5px 5px #E4E2E2',
  },
  
  title:{
    fontSize:'22px',
    fontWeight:'bold',
    color:'#006439',
    display:'flex',
    alignItems:'center',
    width:'100%',
    marginBottom:'30px',
  },
  photo:{
    display: 'block',
    width: '150px',
    height: '150px',
    marginRight: '6%', 
    borderRadius: '50%',
    overflow: 'hidden',
  },
  photoImg:{
    top: '0',
    bottom: '0',
    right: '0',
    left: '0',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center',
  },
  leftColStyle:{
    height:'40px',
    textAlign:'right',
    paddingRight:'15px',
    color:'#8A8A8A'
  },
  rightColStyle:{
    height:'38px',
  },
  btnStyle: {
    width: '150px',
    height: '38px',
    backgroundColor: '#00643C',
    borderRadius: '10px',
    color: 'white',
    float:'right',
  },

};


const layout = {
  labelCol: { span: 6},
  wrapperCol: { span: 18 },
};

const mapStateToProps = state => {
  return {
    
  };
};

const mapDispatchToProps = dispatch => {
  return {
    POST_food(payload) {
      dispatch({ type: 'food/POST_food',payload });
    },
    GET_foods() {
      dispatch({ type: 'food/GET_foods' });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class foodCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {


  }

  handleCreate=(value)=>{
    console.log("in")
    console.log(value)
    const { POST_food, GET_foods} = this.props;
    console.log(POST_food)
    POST_food(value);
    GET_foods()
    this.props.history.push('/food')
  }
  
  disabledDate(current) {
    return current && current < moment().endOf('day');
  }
 
  render() {
    const { history} = this.props;
    
    const { Option } = Select;

    const statusList=[
      {
        id:7,
        value:7,
        name:"星早餐",
        category:2
      }, 
      {
        id:8,
        value:8,
        name:"星想餐",
        category:2
      },
      {
        id:9,
        value:9,
        name:"午茶那堤組合",
        category:2
      },
      {
        id:10,
        value:10,
        name:"輕食",
        category:2
      },
      {
        id:11,
        value:11,
        name:"麵包/點心",
        category:2
      },
      {
        id:12,
        value:12,
        name:"蛋糕甜點",
        category:2
      },
      {
        id:13,
        value:13,
        name:"包裝食品",
        category:2
      },
      {
        id:14,
        value:14,
        name:"蛋糕預購",
        category:2
      },
      {
        id:15,
        value:15,
        name:"新鮮現烤",
        category:2
      },
      {
        id:16,
        value:16,
        name:"中秋節預購",
        category:2
      }
    ]
    return (
      <React.Fragment>
        <div style={{width: '100%', height: '100%',display:'flex',justifyContent:'center',overflowY:'auto',paddingTop:'30px'}}>
          <div style={styles.formStoreUpdate} >
            <div style={styles.title} onClick={() => history.push('/food')} >
              <img 
                src={Images.back} 
                
                style={{width:'25px',height:'18px',marginRight:'10px',cursor:'pointer'}}
                
              />
              返回餐點列表
            </div>


            <div style={{width:'100%'}}>
              <Form
                {...layout}
                name="basic"
                style={{width:'90%',margin:'auto'}}
                onFinish={value=>this.handleCreate(value)}
              >
                <FormInputSelect
                  title="餐點類別"
                  required
                  options={statusList}
                  propName='typeId'
                  placeholder="請選擇類別"
                  requiredErrorMessage="請選擇類別"
                />

                <FormInput
                  required
                  title="餐點名稱"
                  propName='foodName'
                  placeholder="餐點名稱"
                  requiredErrorMessage="請輸入餐點名稱"
                />
                <FormInput
                  required
                  title="餐點英文名稱"
                  propName='foodEnglishName'
                  placeholder="餐點英文名稱"
                  requiredErrorMessage="請輸入餐點英文名稱"
                />
                <FormInput
                  required
                  title="餐點價格"
                  propName='foodPrice'
                  placeholder="餐點價格"
                  requiredErrorMessage="請輸入餐點價格"
                />
                <Form.Item name='foodDescription' label="餐點簡介"  rules={[{ required: true, message: '請輸入餐點簡介' }]}>
                  <textarea style={{resize:'none',height:'100px',width:'100%',borderRadius:'8px',border:'1px solid #00643C'}}/>
                </Form.Item>

                <Button style={styles.btnStyle} htmlType="submit">
                  新增
                </Button>

              </Form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
);
