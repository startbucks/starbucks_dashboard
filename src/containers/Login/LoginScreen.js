import { Component } from 'react';
import { connect } from 'dva';
import _ from 'lodash';
import { Images } from '../../theme';
import { Row, Col, Button, Form, Input } from 'antd';


const styles={
  root: {
    flexGrow: 1,
    height: '100%',
    overflowY: 'hidden',
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    backgroundImage:`URL(${Images.background})`,
    backgroundSize:'cover'
    
  },
  card:{
    width:'35vw',
    height:'auto',
    backgroundColor:'rgba(0,0,0,0.7)',
    display:'flex',
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'space-evenly',

  },
  formStyle:{
    width:'60%',
  },
  inputStyle:{
    width:'100%',
    height:'25px',
    border:'0px',
    // borderBottom:'1px solid #d6d6d6',
    // backgroundColor:'transparent',
    borderRadius:'5px',
    color:'black',
    // marginLeft:'5%'
  },
  btnStyle:{
    width:'100%',
    height:'40px',
    border:'0px',
    borderRadius:'5px',
    backgroundColor:'#00643C',
    color:'white',
    fontSize:'16px',
    fontWeight: '800',
    display:'flex',
    alignItems:'center',
    justifyContent:'center',
    marginTop:'30px',
    marginBottom:'50px',
  }
};

const mapStateToProps = state => {
  return {
    token: _.get(state, 'login.token'),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login(payload) {
      dispatch({ type: 'login/login' ,payload});
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class LoginScreen extends Component {
    constructor(props) {
      super(props);

      this.state = {
        loading: false,
      };
    }

    componentDidMount() {

    }

    handleLogin = value=>{
      console.log(value)
      const {login}=this.props
      login(value)
    }

    render() {
      console.log('Images =>', Images.logo);
      
      return (
        <div style={styles.root}>
          <div style={styles.card}>
            <img src={Images.logo} style={{marginTop:'50px',marginBottom:'20px',display:'block',width:'50%'}}/>
            <Form 
              name='basic'
              initialValues={{
                account: '',
                password: '',
              }}
              onFinish={this.handleLogin}
              style={styles.formStyle}
            >
              <Row style={{alignItems:'center',height:'50px'}}>
                <Col xs={6} sm={6} md={6} lg={6} xl={6} style={{color:'white'}}>
                  帳號
                </Col>
                <Col xs={18} sm={18} md={18} lg={18} xl={18}>
                  <Form.Item
                    name="account"
                  >
                    <input name='account' style={styles.inputStyle}/>
                  </Form.Item>
                </Col>
              </Row>
              <Row style={{alignItems:'center',height:'50px'}}>
                <Col xs={6} sm={6} md={6} lg={6} xl={6} style={{color:'white'}}>
                  密碼
                </Col>
                <Col xs={18} sm={18} md={18} lg={18} xl={18}>
                  <Form.Item
                    name="password"
                  >
                    <input name='password' type="password" style={styles.inputStyle}/>
                  </Form.Item>
                </Col>
              </Row>
              {/*<div style={{ marginBottom:'10%',marginTop:'20%',color:'white'}}>
                帳號<input name='account' style={styles.inputStyle}/>
              </div>
              <div style={{ marginBottom:'10%',color:'white'}}>
                密碼<input name='password' style={styles.inputStyle}/>
            </div>*/}
              
              <Button style={styles.btnStyle} htmlType='submit'>
                登入
              </Button>
            </Form>
          </div>
      </div>
      );
    }
  },
);
