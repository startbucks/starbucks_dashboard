import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {Link } from "dva/router";
import {  Images } from '../../theme';
import { Table, Tag, Space } from 'antd';
import _ from 'lodash';
import moment from 'moment';


const styles = {
  root: {
    flexGrow: 1,
    height: '100%',
    overflowY: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    overflowY: 'auto'
  },
  fullHeight: {
    height: '100%',
  },
  wrapper: {
    width: '90%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  contentBottom: {
    width: '100%',
    marginTop: '80px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    fontSize:'20px',
    fontWeight:'bold'
  },
  contentBottomTitle:{
    marginLeft:'5px',
    marginBottom:'10px',
    display:'flex',
    alignItems:'center',
    cursor:'pointer'
  }
};

const mapStateToProps = state => {
  return {
    activityData: _.get(state, 'activity.activityData'),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_activities() {
      dispatch({ type: 'activity/GET_activities' });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class activityScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {
    const {GET_activities,activityData} =this.props;
    GET_activities()
    console.log("activityData=>",activityData)

  }

  

  render() {
    const { activityData } = this.props;

    const columns = [
      {
        width: '30%',
        title: '活動開始日期',
        dataIndex: 'activityStartDate',
        key: 'activityStartDate',
        align:'center',
        render: (value, record) => (
          <div>
            {(moment(record.activityStartDate).format('YYYY-MM-DD'))}
          </div>
          
        ),
      },
      {
        width: '30%',
        title: '活動結束日期',
        dataIndex: 'activityEndDate',
        key: 'activityEndDate',
        align:'center',
        render: (value, record) => (
          <div>
            {(moment(record.activityEndDate).format('YYYY-MM-DD'))}
          </div>
        ),
      },
      {
        width: '30%',
        title: '活動名稱',
        dataIndex: 'activityName',
        key: 'activityName',
        align:'center',
        render: text => <div style={{ fontWeight: '500', color: '#252525' }}>{text}</div>,
      },
      {
        width: '10%',
        title: '設定',
        dataIndex: 'activityId',
        key: 'activityId',
        align:'center',
        render: (value, record) => 
          
            <img 
              src={Images.edit} 
              onClick={()=>this.props.history.push(`/activity/detail/${record.activityId}`)}>
            </img>
          
      },
    ];

    return (
      <React.Fragment>
        <div style={{ width: '100%', height: '100%' }}>
          <div style={styles.root}>
            <div style={styles.wrapper}>
              <div style={styles.contentBottom}>
                <div style={styles.contentBottomTitle} onClick={()=>this.props.history.push('/activity/create')}>
                  <img src={Images.insert}  style={{width:'25px',marginRight:'10px'}} />新增活動
                </div>
                <Table columns={columns} dataSource={activityData} style={{ width: '100%',textAlign:'center'}} />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
);
