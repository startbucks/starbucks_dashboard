import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {Form , Button, Radio, DatePicker, message,Input} from 'antd';
import { bindActionCreators } from 'redux';
import {  Images } from '../../theme';
import moment from 'moment';
import { FormInput,FormInputSelect } from '../../components';
import hashHistory from '../../utils/hashHistory';

const styles = {
  formStoreUpdate:{
    width:'70%',
    height:'500px',
    marginBottom:'5%',
    display:'flex',
    justifyContent:'flex-start',
    alignItems:'center',
    flexWrap:'wrap',

    backgroundColor:'#FFFFFF',  
    padding:'7%',
    paddingTop:'3%',
    boxShadow:'5px 5px #E4E2E2',
  },
  
  title:{
    fontSize:'22px',
    fontWeight:'bold',
    color:'#006439',
    display:'flex',
    alignItems:'center',
    width:'100%',
    marginBottom:'30px',
  },
  photo:{
    display: 'block',
    width: '150px',
    height: '150px',
    marginRight: '6%', 
    borderRadius: '50%',
    overflow: 'hidden',
  },
  photoImg:{
    top: '0',
    bottom: '0',
    right: '0',
    left: '0',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center',
  },
  leftColStyle:{
    height:'40px',
    textAlign:'right',
    paddingRight:'15px',
    color:'#8A8A8A'
  },
  rightColStyle:{
    height:'38px',
  },
  btnStyle: {
    width: '150px',
    height: '38px',
    backgroundColor: '#00643C',
    borderRadius: '10px',
    color: 'white',
    float:'right',
  },

};

const layout = {
  labelCol: { span: 6},
  wrapperCol: { span: 18 },
};


const mapStateToProps = state => {
  return {

  };
};

const mapDispatchToProps = dispatch => {
  return {
    POST_activity(payload) {
      dispatch({ type: 'activity/POST_activity',payload });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)( 
class activityCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }
  static propTypes = {
    class: PropTypes.object,
  };

  componentDidMount() {


  }

  
  disabledDate(current) {
    return current && current < moment().endOf('day');
  }

  handleCreate=(value)=>{
    
    let temp=[];
    temp={
      activityName:value.activityName,
      activityStartDate:moment(value.activityStartDate).format('YYYY-MM-DD'),
      activityEndDate:moment(value.activityEndDate).format('YYYY-MM-DD'),
      adminId:"1",
      createAt:moment().format('YYYY-MM-DD'),
      activityContent:value.activityContent
    }
    console.log(temp)
    const { POST_activity } = this.props;
    console.log(this)
    POST_activity(temp);
    this.setState({
      loading: true,
    });
    const callback = () => {
      this.setState({
        loading: false,
      })
    }
      
    
    // this.props.history.push('/activity')
  }

  render() {
    return (
      <React.Fragment>
        <div style={{width: '100%', height: '100%',display:'flex',justifyContent:'center',alignItems:'center',overflowY:'auto',paddingTop:'30px'}}>
          <div style={styles.formStoreUpdate}>
            <div style={styles.title} onClick={()=>this.props.history.push('/activity')}>
              <img 
                src={Images.back} 
                style={{width:'25px',height:'18px',marginRight:'10px',cursor:'pointer'}}
              />
              返回活動列表
            </div>


            <div style={{width:'100%'}}>
              <Form
                {...layout}
                name="basic"
                style={{width:'90%',margin:'auto'}}
                onFinish={value=>this.handleCreate(value)}
              >
                <Form.Item
                  required
                  label="活動開始日期"
                  name='activityStartDate'
                  placeholder="活動開始日期"
                  requiredErrorMessage="請輸入日期" 
                >
                  <DatePicker name="activityStartDate" disabledDate={this.disabledDate} />          
                </Form.Item>

                <Form.Item
                  required
                  label="活動結束日期"
                  name='activityEndDate'
                  placeholder="活動結束日期"
                  requiredErrorMessage="請輸入日期" 
                >
                  <DatePicker name="activityEndDate" disabledDate={this.disabledDate} />          
                </Form.Item>

                <FormInput
                  required
                  title="活動名稱"
                  propName='activityName'
                  placeholder="活動名稱"
                  requiredErrorMessage="請輸入活動名稱"
                />
                
                <Form.Item name='activityContent' label="活動簡介" rules={[{ required: true, message: '請輸入活動簡介' }]}>
                  <textarea name='activityContent' style={{resize:'none',height:'100px',width:'100%',borderRadius:'5px',border:'1px solid #00643C'}}/>
                </Form.Item>

                <Button style={styles.btnStyle} htmlType="submit">
                  新增
                </Button>

              </Form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
);
