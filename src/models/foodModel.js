import {
  GET_food,
  GET_foods,
  PATCH_food,
  POST_food
} from '../services/foodService';
import { setToken,getToken } from '../utils/authorization';

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  namespace: 'food',
  state: {
    foodData:[],
    oneFood:{},
  },
  effects: {
    *GET_foods({}, {call, put}) {
      try {
        const token= yield call(getToken)
        const response = yield call(GET_foods,token);
        yield put({type: 'SAVE_food', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
    *GET_food({id}, {call, put}) {
      try {
        console.log("model payload=>",id);
        const token= yield call(getToken)
        const response = yield call(GET_food,id,token);
        yield put({type: 'SAVE_oneFood', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
    *POST_food({payload}, {call, put}) {
      try {
        console.log("payload=>",payload);
        const token= yield call(getToken)
        const response = yield call(POST_food,payload,token);
        console.log("onefoodData=>",response);
        yield put({type: 'SAVE_food', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
  
    *PATCH_food({id,payload}, {call, put}) {
      try {
        const token= yield call(getToken)
        console.log(token)
        console.log("payload=>",payload)
        const response = yield call(PATCH_food,id,payload,token);
        console.log("activityData=>",response);
        yield put({type: 'SAVE_oneFood', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
  },
  
  
  reducers: {
    SAVE_food(state, {payload}) {
      return {
        ...state,
        foodData: payload,
      };
    },
    SAVE_oneFood(state, {payload}) {
      return {
        ...state,
        oneFood: payload,
      };
    },
  },
};
