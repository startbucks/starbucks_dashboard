import {
  POST_activity,
  GET_activities,
  GET_oneActivity,
  PATCH_activity
} from '../services/activityService';
import { setToken,getToken } from '../utils/authorization';

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  namespace: 'activity',
  state: {
    activityData:[],
    oneActivity:{}
  },
  effects: {
    *GET_activities({}, {call, put}) {
      try {
        const token= yield call(getToken)
        const response = yield call(GET_activities,token);
        yield put({type: 'SAVE_activity', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
    *GET_oneActivity({id}, {call, put}) {
      try {
        const token= yield call(getToken)
        const response = yield call(GET_oneActivity,id,token);
        console.log("response=>",response.data.data )
        yield put({type: 'SAVE_oneActivity', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
    *POST_activity({payload}, {call, put}) {
      try {
        const token= yield call(getToken)
        const response = yield call(POST_activity,payload,token);
        console.log("activityData=>",response);
        
      } catch (err) {
        throw err;
      }
    },
    *PATCH_activity({id,payload}, {call, put}) {
      try {
        const token= yield call(getToken)
        console.log(token)
        console.log("payload=>",payload)
        const response = yield call(PATCH_activity,id,payload,token);
        console.log("activityData=>",response);
        yield put({type: 'SAVE_oneActivity', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
  },
  
  reducers: {
    SAVE_activity(state, {payload}) {
      return {
        ...state,
        activityData: payload,
      };
    },
    SAVE_oneActivity(state, {payload}) {
      return {
        ...state,
        oneActivity: payload,
      };
    },
  },
};
