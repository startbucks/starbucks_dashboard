import {
    login,
  } from '../services/loginService';
  import { setToken,getToken,cleanToken } from '../utils/authorization';
  import router from 'dva/router';
  
  // eslint-disable-next-line import/no-anonymous-default-export
  export default {
    namespace: 'login',
    state: {
        token:""
    },
    effects: {
      *login({payload}, {call, put}) {
        try {
          console.log(payload)
          const response = yield call(login,payload);
          console.log(response.data.token)
          yield put({type: 'SAVE_token', payload: response.data.token});
          yield setToken(response.data.token);
          // yield setToken();
          window.location.reload()
        } catch (err) {
          throw err;
        }
      },
      *logout({callback,loading}, {call, put}) {
        try {
          if (loading) loading(true);
            // 登出會員
            yield cleanToken();
            // 導向登入頁面
            if (loading) loading(false);
            // yield router.push('/');
            window.location.reload();
            if (callback) callback();
        } catch (err) {
          console.log(err);
        }
      },
    },
    reducers: {
        SAVE_token(state, {payload}) {
          return {
            ...state,
            token: payload,
          };
        },
      },
  };
  