import {
  GET_drinks,
  POST_drink,
  PATCH_drinks,
  GET_oneDrink,
} from '../services/drinkService';
import {setToken, getToken} from '../utils/authorization';

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  namespace: 'drink',
  state: {
    drinkData: [],
  },
  effects: {
    *GET_drinks({}, {call, put}) {
      try {
        const token = yield call(getToken);
        const response = yield call(GET_drinks, token);
        console.log('res==>', response.data.data);
        yield put({type: 'SAVE_drink', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
    *GET_oneDrink({id}, {call, put}) {
      try {
        const token = yield call(getToken);
        const response = yield call(GET_oneDrink, id, token);
        console.log("response",response.data.data);
        yield put({type: 'SAVE_oneDrink', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
    *POST_drink({payload}, {call, put}) {
      try {
        console.log('in');
        const token = yield call(getToken);
        console.log(token);
        const response = yield call(POST_drink, payload, token);
        console.log('drinkData=>', response);
      } catch (err) {
        throw err;
      }
    },
    *PATCH_drinks({id, payload}, {call, put}) {
      try {
        console.log(payload)
        const token = yield call(getToken);
        const response = yield call(PATCH_drinks, id, payload, token);
        yield put({type: 'SAVE_oneDrink', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
  },

  reducers: {
    SAVE_drink(state, {payload}) {
      return {
        ...state,
        drinkData: payload,
      };
    },
    SAVE_oneDrink(state, {payload}) {
      return {
        ...state,
        oneDrink: payload,
      };
    },
  },
};
