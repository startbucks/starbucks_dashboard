import {
  GET_company,
  PATCH_company
} from '../services/appService';
import { setToken,getToken } from '../utils/authorization';

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  namespace: 'company',
  state: {
    companyData:[]
  },
  effects: {
    *GET_company({}, {call, put}) {
      try {
        const token= yield call(getToken)
        const response = yield call(GET_company,token);
        yield put({type: 'SAVE_company', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
    *PATCH_company({payload}, {call, put}) {
      try {
        const token= yield call(getToken)
        console.log(token)
        console.log("payload=>",payload)
        const response = yield call(PATCH_company,payload,token);
        console.log("companyData=>",response);
        yield put({type: 'SAVE_company', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
  },
  
  reducers: {
    SAVE_company(state, {payload}) {
      return {
        ...state,
        companyData: payload,
      };
    },
  },
};
