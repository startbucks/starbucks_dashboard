import React, { Component } from "react";
import { Route, Switch, routerRedux, withRouter } from "dva/router";
import UnRoutePage from './routes/UnRoutePage';
import RoutePage from './routes/RoutePage';
import 'antd/dist/antd.css';
import _ from 'lodash';


const { ConnectedRouter } = routerRedux;

class Root extends Component {
  render() {
    const { children } = this.props;
    return children;
  }
}

const RouterRoot = withRouter(Root);

// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  return (
    <ConnectedRouter history={props.history}>
      <RouterRoot {...props}>
        <Switch>        
          {!_.isEmpty(localStorage.token) ? <RoutePage history={props.history}/> : <UnRoutePage history={props.history}/>}
          {/*<RoutePage history={props.history}/>
          <Route path="/" exact component={UnRoutePage} />
          <Route path="/login" component={LoginScreen} />*/}
        </Switch>
      </RouterRoot>
    </ConnectedRouter>
  );
};
